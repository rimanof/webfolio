$(document).ready(function(){                                           //executa urmatoarele instructiuni odata ce documentul s-a incarcat...

    let header= $(".header");                                           //salveaza div-ul cu clasa header
    let intro= $(".intro");                                             //salveaza div-ul cu clasa intro
    let headerH= header.height();                                       //salveaza distanta de sus la fereastra pana la sfarsitul div-ului header
    let introH= intro.innerHeight();                                    //salveaza distanta de sus la fereastra pana la sfarsitul div-ului intro
    let sections = $("section");                                        //salvez toate sectiunile din document
    let navLi=$(".header_link")                                         //salvez toate linkurile din document
    let current= '';                                                    //declar variabila care va memoriza id-ul sectiunii in care ma aflu

    /*header*/
    $(window).on("scroll load resize",function (event) {                //executa o serie de instructiuni la scrolare, incarcare sau redimensionare pagina
        let scroll = $(window).scrollTop();                             //salveaza pozitia cat s-a dat scroll la pagina
        fixMenu(scroll);                                                //verific daca meniul a trecut de blocul intro

        /*scroll section */
        $.each(sections, function(key, section){                        //parcurg toate sectiunile returnate din document
            const sectionTop = section.offsetTop;                       //obtin distanta de la incaputul paginii pana la fiecare sectiune in parte
            if(scroll+70 >= sectionTop){                                //verific daca ma aflu pe sectiunea data
                current = section.id;                                   //daca ma aflu atunci variabila current va memoriza id-ul sectiunei curente
            }
        });

        let activeL = $(".header__active");                             //returnez linkul active
        activeL.removeClass("header__active");                          //elimin clasa active pentru li activ

        $.each(navLi,function(key, a){                                  //parcurg fiecare link din meniu
            let liClas = a.className;                                   //returnez numele clasei la fiecare link din meniu
            if($(window).scrollTop() + $(window).height() >= $(document).height()-10) {    //verific daca pagina nu a fost scrolata pana la capat
            $(".header_link:last").addClass("header__active");                       //daca da atunci adaug clasa header__active pentru ultimul element din lista
            }
            else{                                                       //daca nu...
                if(liClas.indexOf(current)>=0){                         //verific daca id-ul selectat mai sus face parte din stringul cu clasele linkului
                    $(this).addClass("header__active");                 //daca da, adaug clasa header__active pentru elementul corespunzator
                }
            }
        });

        burgerShow();                                                   //apeleaza functia care afiseaza burger-ul daca marimea ferestrei este mai mica ca 990
        burgerHide();                                                   //apeleaza functia care ascunde burger-ul daca marimea ferestrei este mai mare ca 990
    });

    /*smoth scroll*/                                        //poate functiona si prin href="#id_bloc" la link
    $("[data-scroll]").on("click",function(event){                      //la click pe elemente data-scroll indeplinesc functia
        event.preventDefault();                                         //elimin evenimentele precedente a linkului
        let elementID= $(this).data("scroll");                          //obtin din data-scroll id-ul div-ului la care vreau sa ma cobor
        let elementTop= $('#'+elementID).offset().top;                  //obtin distanta de sus pana la elementul la care vreau sa cobor

        $("html, body").animate({                                       //adaug o animatie pentru html si body pentru a scrola pana la elementul returnat
            scrollTop: elementTop                                       //
        },"slow");                                                      //viteza animatiei "slow"

        burgerShow();                                                   //apeleaza functia care afiseaza burger-ul daca marimea ferestrei este mai mica ca 990
    });

    /*home click*/
    $(".header_link").eq(0).on("click",function(event){                 //la click pe primul element de clasa .header_link voi adauga animatia scrollTop
        event.preventDefault();                                         //elimin evenimentele precedente a linkului
        $("html, body").animate({                                       //adaug animatie pentru a scrola pagina la inceput
            scrollTop: 0
        },"slow");                                                      //viteza animatiei "slow"

        burgerShow();                                                   //apeleaza functia care afiseaza burger-ul daca marimea ferestrei este mai mica ca 990
    });

    //colored slider (before, after)
    let sliders = $(".skills__slider");                                 //returnez toate slider-urile de clasa .skills__slider
    let percents = [];                                                  //initializez array
    for (let i = 0; i < sliders.length; i++){                           //parcurg fiecare slide in parte
        percents[i] = $(".skills__percent").eq(i).text();               //returnez textul cu lungimea slider-ului

        var styleElem = document.head.appendChild(document.createElement("style"));             //initializez o variabila de tip style di o adaug in document
        styleElem.innerHTML = '#'+sliders[i].id+':before {width: '+percents[i]+';}!important';  //adaug style-urile pentru before
        var styleElem = document.head.appendChild(document.createElement("style"));             //initializez o variabila de tip style di o adaug in document
        styleElem.innerHTML = '#'+sliders[i].id+':after {left: '+percents[i]+';}!important';    //adaug style-urile pentru after
    };

    $(".burger__open").on("click",function(){                           //la cllick pe iconita cu trei linii paralele se va deschide meniul pe tot ecranul
        header.addClass("header__show");                                //adaug clasa header show care afiseaza meniul pe intregul ecran
        $(".burger__open").css("display","none");                       //ascund iconita cu trei linii paralele
        $(".burger__close").css("display","block");                     //afisez iconita cu X
    });

    $(".burger__close").on("click",function(){                          // la click pe iconita X voi ascunde meniul si voi afisa iconita cu trei linii paralele
        burgerShow();                                                   //apeleaza functia care afiseaza burger-ul daca marimea ferestrei este mai mica ca 990

        let scroll = $(window).scrollTop();                             //salveaza pozitia cat s-a dat scroll la pagina
        fixMenu(scroll);                                                //verific daca meniul a trecut de blocul intro
    });
    

    function fixMenu(scroll){                                           //verific daca meniul a trecut de blocul intro
        if(scroll+70 > introH && !header.hasClass("header__show")){     //verifica daca blocul intro a fost scrolat
            header.addClass("header__fixed");                           //adaugam clasa div-ului header
        }
        else{                                                           //daca blocul intro se afla mai jos atunci punem meniul la inapoi sus
            header.removeClass("header__fixed");                        //stergem clasa pentru div-ul header
        }
    };


    function burgerShow(){                                              //afisaz iconita cu trei linii paralele daca marimea documentului este mai mica de 990
        const w_width=$(document).width();                              //returnez marimea documentului
        if(w_width<=990){                                                //verific daca marimea documentului este mai mica de 990
            header.removeClass("header__show");                         //elimin clasa header__Show
            $(".burger__open").css("display","block");                  // afisez iconita burger
            $(".burger__close").css("display","none");                  //elimin iconita X
        }
    }

    function burgerHide(){                                              //ascund burger daca marimea documentului este mai mare ca 990
        const w_width=$(document).width();                              //returnez marimea documentului
        if(w_width>990){                                                //verific daca marimea documentului este mai mare de 990
            header.removeClass("header__show");                         //elimin clasa header__Show
            $(".burger__open").css("display","none");                   //elimin iconita burger
            $(".burger__close").css("display","none");                  //elimin iconita X
        }
    };
    

    //E-mail ajax send
    $("form").submit(function(){ //Change
        var th = $(this);
        $.ajax({
            type: "POST",
            url: "php/mail.php",    //Change
            data: th.serialize()
        }).done(function() {
            alert("Thank you!");
            //Done function
            setTimeout(function(){
                th.trigger("reset");
            }, 1000);
        });
    });
})